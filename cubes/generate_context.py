import configparser


def gen_db_configs(*args, **kwargs):
    config = configparser.ConfigParser()
    config.read('db_config.ini')
    schema = config['db']['schema']
    table = config['db']['table']

    return {
        'data_table': {
            'schema': schema,
            'table': table
        }
    }
