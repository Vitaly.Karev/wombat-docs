import logging
from typing import Tuple

import isoweek
import pandas as pd
from wombat.cubes import NamedOutput

from cubes.common import BaseDbCube, BaseProjectCube
from src.w2v_learn import create_model, to_ms_style, train_w2v_by_category, azbq_w2v_query_ms_map


class W2VLearn(BaseDbCube, BaseProjectCube):
    SG_ALGORITHM = 1
    N_COMPS = 25
    WINDOW_SIZE = 6
    MIN_COUNT = 1
    WORKERS = 8

    _ALLOW_CACHE_OUTPUTS = True

    _named_input_types = {
        'cities': Tuple[int]
    }

    @property
    def w2v_model(self):
        return create_model(
            sg=W2VLearn.SG_ALGORITHM,
            size=W2VLearn.N_COMPS,
            window=W2VLearn.WINDOW_SIZE,
            min_count=W2VLearn.MIN_COUNT,
            workers=W2VLearn.WORKERS
        )

    @property
    def category(self):
        return self._context['category'].copy()

    @property
    def cities(self):
        return self.named_inputs['cities']

    @property
    def start_date(self):
        return isoweek.Week(year=self.week_start // 100, week=self.week_start % 100).day(0)

    @property
    def end_date(self):
        return isoweek.Week(year=self.week_end // 100, week=self.week_end % 100).day(6)

    def _execute(self) -> NamedOutput:
        print('Starting with:'
              f'    week_start={self.week_start},'
              f'    week_end={self.week_end},'
              f'    date_start={self.start_date},'
              f'    date_end={self.end_date},'
              f'    category={self.category}')
        azbq_query = self.data_table.get_azbq_w2v_query(
            wgh1_ids=self.category[2],
            start_date=self.start_date,
            end_date=self.end_date,
            cities=self.cities
        )
        azbq_w2v = to_ms_style(pd.read_sql_query(azbq_query, self.conn), azbq_w2v_query_ms_map)

        w2v_res = train_w2v_by_category(azbq_data=azbq_w2v,
                                        w2v_model=self.w2v_model,
                                        update=False)

        return NamedOutput({'model': w2v_res[0],
                            'vectors': w2v_res[1],
                            'cos_dist': w2v_res[2]})
