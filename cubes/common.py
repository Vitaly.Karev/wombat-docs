import os
import json
from typing import Optional, Union, Tuple, List

from isoweek import Week
from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
from wombat.cubes import BaseCube, Output, NamedOutput

from src.model import BrowsingTable
from src.support.weeks import get_prev_week


class BaseDbCube(BaseCube):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__conn = None

    @property
    def data_table(self):
        return create_table(schema=self._context['data_table']['schema'],
                            table=self._context['data_table']['table'])

    @property
    def conn(self):
        if self.__conn is None:
            conn_str = get_conn_str(db_config=self._context['db'])
            self.__conn = create_engine(conn_str, poolclass=NullPool)

        return self.__conn


class BaseProjectCube(BaseCube):

    @property
    def week_start(self):
        week = int(self._context['run_params']['week_start'])
        if week == -1:
            week = get_prev_week(Week.thisweek().week, 1)
        return week

    @property
    def week_end(self):
        week = int(self._context['run_params']['week_end'])
        if week == -1:
            week = get_prev_week(Week.thisweek().week, 1)
        return week


class GetMeta(BaseCube):

    def _execute(self):
        return NamedOutput({
            'categories': parse_categories(self._context['categories_path']),
            'cities': parse_cities(self._context['cities_path'])
        })


class PrepareTable(BaseDbCube):

    def _execute(self) -> Optional[Union[Output, NamedOutput, Tuple[Output, NamedOutput], Tuple[NamedOutput, Output]]]:
        if not self.data_table.table_exist(conn=self.conn):
            self.data_table.create_table(conn=self.conn)

        return None


class CheckTable(BaseDbCube, BaseProjectCube):
    _named_input_types = {
        'categories': List[Tuple]
    }

    @property
    def categories(self):
        return self.named_inputs['categories']

    def _execute(self) -> Optional[Union[Output, NamedOutput, Tuple[Output, NamedOutput], Tuple[NamedOutput, Output]]]:
        for category in self.categories:
            self.data_table.check_data(
                self.conn,
                wgh1_ids=category[2],
                week_start=self.week_start,
                week_end=self.week_end
            )

        return None


def create_table(schema, table):
    return BrowsingTable(schema=schema, table=table)


def get_conn_str(db_config):
    conn_str_template = 'postgresql+psycopg2://{db_user}:{db_pass}@{db_url}/{db_name}'

    return conn_str_template.format(db_user=db_config['user'],
                                    db_pass=db_config['pass'],
                                    db_url=db_config['url'],
                                    db_name=db_config['db_name'])


def parse_categories(categories_path):
    files = os.listdir(categories_path)

    categories = []
    for file in files:
        file_path = os.path.join(categories_path, file)
        if os.path.isdir(file):
            continue

        with open(file_path, 'r') as f:
            data = json.load(f)

        categories.append((data['name'], data['wgh2'], tuple(data['wgh1'])))

    return categories


def parse_cities(cities_path):
    with open(cities_path, 'r') as f:
        cities = json.load(f)

    return tuple(cities)
