from typing import List, Tuple

import isoweek
from sqlalchemy import text as query_to_text
from wombat.cubes import NamedOutput

from cubes.common import BaseDbCube, BaseProjectCube
from src.support.weeks import generate_weeks


class CollectMarts(BaseDbCube, BaseProjectCube):
    _ALLOW_CACHE_OUTPUTS = True

    _named_input_types = {
        'categories': List[Tuple],
        'cities': Tuple[int]
    }

    @property
    def n_cats_in_query(self):
        return int(self._context['n_cats_in_query'])

    @property
    def categories(self):
        return self.named_inputs['categories']

    def _execute(self):
        print('Starting with:,'
              f'    table_name={self.data_table.full_name},'
              f'    week_start={self.week_start},'
              f'    week_end={self.week_end}')

        if self.data_table.full_name == 'adb.sbds.w2vec_data':
            raise ValueError(f'You cannot fill table {self.data_table.full_name}. It`s for production!')

        categories_by_weeks = self.get_weeks_for_collect()
        self.fill_table(categories_by_weeks=categories_by_weeks)

        return NamedOutput({'success': True})

    def get_weeks_for_collect(self):
        categories_by_weeks = {}
        for category in self.categories:
            exist_weeks = self.data_table.get_weeks(self.conn, wgh1_ids=category[2])
            if self.week_start == 0:
                last_filled_week = exist_weeks[-1] if len(exist_weeks) else 201701
            else:
                last_filled_week = self.week_start
            weeks_to_fill = generate_weeks(start=last_filled_week, end=self.week_end)

            if not set(exist_weeks).isdisjoint(set(weeks_to_fill)):
                raise ValueError(f'Table contains weeks to collect already.')

            for week in weeks_to_fill:
                categories_by_weeks.setdefault(week, []).append(category)

        return categories_by_weeks

    def fill_table(self, categories_by_weeks):
        for week, categories in categories_by_weeks.items():
            categories_by_chunks = split_categories(categories, block_size=self.n_cats_in_query)
            for chunk_categories in categories_by_chunks:
                wgh1_ids = []
                for category in chunk_categories:
                    wgh1_ids.extend(category[2])

                w = isoweek.Week(week // 100, week % 100)
                start_day, end_day = w.day(0), w.day(6)
                print(chunk_categories)
                print('    ', week, start_day, end_day)
                query = self.data_table.fill_table_query(week=week,
                                                         day_start=start_day,
                                                         day_end=end_day,
                                                         categories=wgh1_ids)
                self.conn.execute(query_to_text(query))


def split_categories(categories: List[Tuple[str, List[int]]], block_size: int) -> List[List]:
    # TODO: split by count of products in category
    return [categories[i:i + block_size] for i in range(0, len(categories), block_size)]
