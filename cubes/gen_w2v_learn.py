from .common import parse_categories
from src.support.weeks import get_prev_week, generate_weeks


def generate_w2v_learn(context, *args, **kwargs):
    week_start = int(context['run_params']['week_start'])
    week_end = int(context['run_params']['week_end'])
    window_size = int(context['window_size']) + 1
    categories = parse_categories(context['categories_path'])

    result = {}
    for cat in categories:
        cat_name = cat[0]

        for week in generate_weeks(week_start, week_end):
            step1 = f'w2vLearn_{cat_name}_{week}_{window_size-1}'
            result[step1] = {
                'exec': 'cubes/learn.py:W2VLearn',
                'context': {
                    'category': cat,
                    'run_params': {
                        'week_start': get_prev_week(week, window_size),  # first week in window
                        'week_end': get_prev_week(week, 1)  # end week in window
                    }
                },
                'named_inputs': {
                    'cities': 'getMeta[cities]'
                },
                'depends_on': ['checkTable']
            }

    return result
