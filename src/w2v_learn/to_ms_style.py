import re

from df_loader.pandas_sql_loader.downcast import downcast
from numpy import int8, int64, float16, float64, dtype

MAP_INIT_TYPES = {
    object: object,
    str: str,
    int64: int8,
    float64: float16
}


def to_ms_style(df, column_map):
    map_types = get_map_types(column_map)
    for col_name, ms_type in map_types.items():
        ms_base = get_dtype_base(str(dtype(ms_type)))
        gp_base = get_dtype_base(str(df[col_name].dtype))
        if ms_base == gp_base:
            continue

        init_type = MAP_INIT_TYPES[ms_type]
        if init_type == str:
            df[col_name] = df[col_name].astype(str)
            continue
        df[col_name] = downcast(arr=df[col_name], init_type=init_type)

    df.rename(columns=get_map_names(column_map), inplace=True)
    return df


def get_dtype_base(s):
    m = re.search(r"\d", s)
    if m is None:
        return s

    return s[:m.start()]


def get_map_names(column_map):
    d = {}
    for k, v in column_map.items():
        d[k] = v[0]
    return d


def get_map_types(column_map):
    d = {}
    for k, v in column_map.items():
        d[k] = v[1]
    return d


azbq_w2v_query_ms_map = {

    }
