import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


def train_w2v_by_category(azbq_data, w2v_model, update):
    sent_timing_clean = prepare_corpus(azbq_data=azbq_data)

    w2v_model.build_vocab(sent_timing_clean, update=update)
    w2v_model.train(sent_timing_clean, total_examples=w2v_model.corpus_count, epochs=w2v_model.epochs)

    w2v_res = get_w2v_vectors(w2v_model=w2v_model)
    return w2v_res


def prepare_corpus(azbq_data):
    azbq_data['watch_time'] = pd.to_datetime(azbq_data['watch_time'], unit='ms')
    azbq_data['session_id'] = azbq_data['session_id']
    ZBQFLD1_list_sorted_curr = list_of_items_in_sess(azbq_data, sort_v='session_id')
    sent_timing_clean = ZBQFLD1_list_sorted_curr.loc[
        (ZBQFLD1_list_sorted_curr.len > 1) & (ZBQFLD1_list_sorted_curr.len < 20), 'ZMATERIAL'].tolist()
    return sent_timing_clean


def get_w2v_vectors(w2v_model):
    word_index = []
    word_list = []
    for key_ in w2v_model.wv.vocab.keys():
        word_index.append(key_)
        word_list.append(w2v_model.wv[key_])

    df_w2v = pd.DataFrame(word_list, index=word_index)
    df_w2v_cos = pd.DataFrame(cosine_similarity(df_w2v, dense_output=False), index=df_w2v.index, columns=df_w2v.index)
    w2v_res = (w2v_model, df_w2v, df_w2v_cos)
    return w2v_res


def list_of_items_in_sess(df, sort_v='session_id', time_v='watch_time'):
    df_sorted = df.sort_values([sort_v, time_v])
    df_sorted['new_session'] = df_sorted[sort_v] != df_sorted[sort_v].shift()
    df_sorted['same_item'] = df_sorted.ZMATERIAL == df_sorted.ZMATERIAL.shift()
    df_sorted['ZMATERIAL'] = df_sorted['ZMATERIAL'].astype('str')
    df_sorted.loc[df_sorted['new_session'], 'same_item'] = False
    ses_zm_list_sorted = df_sorted[~df_sorted['same_item']].sort_values([sort_v, time_v]).groupby(sort_v)[
        'ZMATERIAL'].apply(list).reset_index()
    ses_zm_list_sorted['len'] = ses_zm_list_sorted.ZMATERIAL.apply(lambda x: len(x))
    return ses_zm_list_sorted
