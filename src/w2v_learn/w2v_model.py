from gensim.models.word2vec import Word2Vec


def create_model(sg, size, window, min_count, workers):
    w2v_model = Word2Vec(None, sg=sg, size=size, window=window, min_count=min_count, workers=workers)
    return w2v_model


def load_model(model_path):
    w2v_model = Word2Vec.load(model_path)
    return w2v_model
