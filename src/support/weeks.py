from isoweek import Week


def generate_weeks(start, end):
    weeks = []
    for week in range(start, end + 1):
        year = week // 100
        week_of_year = week % 100
        if week_of_year > Week.last_week_of_year(year).week or week_of_year == 0:
            continue
        weeks.append(week)
    return weeks


def get_prev_week(week, n):
    learn_range = n

    year = week // 100
    week_n = week % 100

    diff = week_n - learn_range

    if diff <= 0:
        year -= 1
        week_n = Week.last_week_of_year(year).week  + diff
        prev_week = year * 100 + week_n
    else:
        prev_week = year * 100 + diff

    return prev_week
