from typing import List

from ..base import BaseTable


class Table(BaseTable):

    def create_table_query(self):
        return f"""blank
        """

    def get_weeks_query(self, wgh1_ids: List[int]):
        wgh1_ids = Table.to_txt(wgh1_ids)
        return f"""blank
        """

    def fill_table_query(self, week, day_start, day_end, categories):
        categories = Table.to_txt(categories)
        return f"""blank
            """

    def get_azbq_w2v_query(self, wgh1_ids, start_date, end_date, cities):
        wgh1_ids = Table.to_txt(wgh1_ids)
        return f"""blank
                """
