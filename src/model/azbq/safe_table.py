from .table import Table
from ..base import BaseSafeTable, SafeTableError
from ...support.weeks import generate_weeks


class SafeTable(BaseSafeTable, Table):

    def check_data(self, conn, wgh1_ids, week_start, week_end):
        self.check_weeks(conn, wgh1_ids, week_start, week_end)
        self.check_distribution(conn, wgh1_ids)

    def get_weeks(self, conn, wgh1_ids):
        cursor = conn.execute(self.get_weeks_query(wgh1_ids))
        weeks = []
        for week in cursor:
            weeks.append(int(week[0]))

        return sorted(weeks)

    def check_weeks(self, conn, wgh1_ids, week_start, week_end):
        exist_weeks = self.get_weeks(conn=conn, wgh1_ids=wgh1_ids)
        normal_weeks = generate_weeks(week_start, week_end)

        diff = set(normal_weeks) - set(exist_weeks)
        if len(diff) == 0:
            return True

        for week in diff:
            okay_missing_weeks(wgh1_ids, week)

    def check_distribution(self, conn, wgh1_ids):
        pass


def okay_missing_weeks(wgh1_ids, week):
    if week in range(201701, 201713):
        return True

    raise SafeTableError(f'Unexpected missing week {week} for these wgh1: {wgh1_ids}')

