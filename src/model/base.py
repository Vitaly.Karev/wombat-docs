from abc import ABC, abstractmethod


class BaseTable(ABC):

    def __init__(self, schema, table):
        self._schema = schema
        self._table = table

    @abstractmethod
    def create_table_query(self, *args, **kwargs):
        pass

    def create_table(self, conn):
        return conn.execute(self.create_table_query())

    def table_exist(self, conn):
        table_catalog, table_schema = self.schema.split('.')
        return conn.has_table(table_name=self.table, schema=table_schema)

    def drop_table(self, conn):
        conn.execute(f"DROP TABLE {self.full_name}")

    @property
    def schema(self):
        return self._schema

    @property
    def table(self):
        return self._table

    @property
    def full_name(self):
        return f"{self.schema}.{self.table}"

    @staticmethod
    def to_txt(values, v_type=str):
        if v_type is str:
            return '(' + ', '.join([f"'{v}'" for v in values]) + ')'
        else:
            return f"({', '.join([str(v) for v in values])})"


class BaseSafeTable(ABC):

    @abstractmethod
    def check_data(self, *args, **kwargs):
        pass


class SafeTableError(Exception):
    pass
